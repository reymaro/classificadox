-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 21-05-2022 a las 02:31:34
-- Versión del servidor: 10.4.12-MariaDB-log
-- Versión de PHP: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `matias_reynolds`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_01_18_085303_create_permission_tables', 1),
(6, '2022_01_18_091702_create_products_table', 1),
(7, '2022_05_18_175343_create_orders_table', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 6),
(2, 'App\\Models\\User', 5),
(3, 'App\\Models\\User', 4),
(3, 'App\\Models\\User', 7),
(3, 'App\\Models\\User', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `orders`
--

INSERT INTO `orders` (`id`, `from`, `to`, `content`, `status`, `created_at`, `updated_at`) VALUES
(7, 8, 7, 'a:5:{s:10:\"product_id\";s:1:\"6\";s:12:\"product_name\";s:34:\"Mochila Foldsack No. 1 p/notebooks\";s:8:\"quantity\";s:1:\"2\";s:7:\"message\";s:20:\"tiene en color rosa?\";s:6:\"amount\";d:210;}', 'entregado', '2022-05-21 02:04:58', '2022-05-21 02:29:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2022-05-18 03:34:56', '2022-05-18 03:34:56'),
(2, 'role-create', 'web', '2022-05-18 03:34:56', '2022-05-18 03:34:56'),
(3, 'role-edit', 'web', '2022-05-18 03:34:56', '2022-05-18 03:34:56'),
(4, 'role-delete', 'web', '2022-05-18 03:34:56', '2022-05-18 03:34:56'),
(5, 'product-list', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(6, 'product-create', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(7, 'product-edit', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(8, 'product-delete', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(9, 'user-list', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(10, 'user-create', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(11, 'user-edit', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(12, 'user-delete', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(13, 'order-list', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(14, 'order-create', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(15, 'order-edit', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(16, 'order-delete', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `tags` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `detail`, `image`, `price`, `tags`, `owner`, `created_at`, `updated_at`) VALUES
(5, 'Camisetas Premium p/hombre', 'Estilo ajustado, manga larga raglán en contraste, tapeta henley de tres botones, peso ligero y tela suave para un uso cómodo y transpirable. Y camisas con costuras sólidas con cuello redondo hechas para mayor durabilidad y un gran calce para la ropa de moda casual y los fanáticos del béisbol acérrimos. El escote redondo estilo henley incluye una tapeta de tres botones.', '20220521012047.jpg', '222.00', 'a:2:{i:0;s:6:\"hombre\";i:1;s:7:\"premium\";}', 7, '2022-05-21 01:20:47', '2022-05-21 01:20:47'),
(6, 'Mochila Foldsack No. 1 p/notebooks', 'Tu mochila perfecta para el día a día y los paseos por el bosque. Guarde su computadora portátil (hasta 15 pulgadas) en la funda acolchada.', '20220521012242.jpg', '105.00', 'a:2:{i:0;s:6:\"hombre\";i:1;s:7:\"mochila\";}', 7, '2022-05-21 01:22:42', '2022-05-21 01:22:42'),
(7, 'Chaqueta de algodón para hombre', 'Grandes chaquetas de abrigo para primavera/otoño/invierno, adecuadas para muchas ocasiones, como trabajo, senderismo, camping, montaña/escalada, ciclismo, viajes u otras actividades al aire libre. Buena opción de regalo para usted o su familiar. Un cálido amor al padre, esposo o hijo en este día de acción de gracias o Navidad.', '20220521012421.jpg', '555.00', 'a:2:{i:0;s:6:\"hombre\";i:1;s:8:\"chaqueta\";}', 7, '2022-05-21 01:24:21', '2022-05-21 01:24:21'),
(8, 'SanDisk SSD PLUS 1TB Internal SSD - SATA III 6 Gb/s', 'Fácil actualización para un arranque, apagado, carga de aplicaciones y respuesta más rápidos (en comparación con el disco duro SATA de 2,5\" a 5400 RPM; según las especificaciones publicadas y las pruebas comparativas internas que utilizan las puntuaciones PCMark vantage) Aumenta el rendimiento de escritura en ráfaga, lo que lo hace ideal para cargas de trabajo típicas de PC El equilibrio perfecto entre rendimiento y confiabilidad Velocidades de lectura/escritura de hasta 535 MB/s/450 MB/s (basado en pruebas internas; el rendimiento puede variar según la capacidad de la unidad, el dispositivo host, el sistema operativo y la aplicación).', '20220521012533.jpg', '256.00', 'a:1:{i:0;s:11:\"informatica\";}', 7, '2022-05-21 01:25:33', '2022-05-21 01:25:33'),
(9, 'Samsung 49-Inch CHG90 144Hz Curved Gaming Monitor (LC49HG90DMNXZA) – Super Ultrawide Screen QLED', 'MONITOR DE JUEGOS CURVADO SUPER ULTRAWIDE 32:9 DE 49 PULGADAS con pantalla dual de 27 pulgadas una al lado de la otra TECNOLOGÍA QUANTUM DOT (QLED), compatibilidad con HDR y calibración de fábrica que proporciona un contraste y un color increíblemente realistas y precisos ALTA VELOCIDAD DE REFRESCO DE 144 HZ y un tiempo de respuesta ultrarrápido de 1 ms elimine el desenfoque de movimiento, el efecto fantasma y reduzca el retraso de entrada', '20220521012646.jpg', '788.00', 'a:1:{i:0;s:7:\"monitor\";}', 7, '2022-05-21 01:26:46', '2022-05-21 01:26:46'),
(10, 'John Hardy Legends Naga - Pulsera p/mujer', 'De nuestra colección Legends, el Naga se inspiró en el mítico dragón de agua que protege la perla del océano. Úselo mirando hacia adentro para recibir amor y abundancia, o hacia afuera para protección.', '20220521012839.jpg', '123.00', 'a:2:{i:0;s:5:\"mujer\";i:1;s:7:\"joyeria\";}', 7, '2022-05-21 01:28:39', '2022-05-21 01:28:39'),
(11, 'Chaqueta de snowboard 3 en 1 para mujer', 'Las chaquetas son de tamaño estándar de EE. UU., elija el tamaño como su uso habitual Material: 100% poliéster; Tejido del forro desmontable: forro polar cálido. Forro funcional desmontable: respetuoso con la piel, ligero y cálido. Chaqueta con forro de cuello alto, te mantiene caliente en climas fríos. Bolsillos con cremallera: 2 bolsillos con cremallera para las manos, 2 bolsillos con cremallera en el pecho (suficientes para guardar tarjetas o llaves) y 1 bolsillo oculto en el interior. Los bolsillos con cremallera para las manos y el bolsillo oculto mantienen tus cosas seguras. Diseño humanizado: Capucha ajustable y desmontable y puño ajustable para evitar el viento y el agua, para un ajuste cómodo. El diseño desmontable 3 en 1 brinda más comodidad, puede separar el abrigo y el interior según sea necesario, o usarlos juntos. Es adecuado para diferentes estaciones y te ayuda a adaptarte a diferentes climas.', '20220521013052.jpg', '456.00', 'a:2:{i:0;s:8:\"chaqueta\";i:1;s:5:\"mujer\";}', 7, '2022-05-21 01:30:52', '2022-05-21 01:30:52'),
(12, 'Chubasquero de mujer cortavientos', 'Perfet ligero para viajes o uso casual --- Manga larga con capucha, diseño de cintura con cordón ajustable. Chubasquero con cierre frontal de botón y cremallera, completamente forrado a rayas y El chubasquero tiene 2 bolsillos laterales son de buen tamaño para guardar todo tipo de cosas, cubre las caderas y la capucha es amplia pero no exagera. Los cordones ajustables le dan un estilo real.', '20220521013217.jpg', '666.00', 'a:2:{i:0;s:5:\"mujer\";i:1;s:6:\"abrigo\";}', 7, '2022-05-21 01:32:17', '2022-05-21 01:32:17'),
(13, 'Acer SB220Q bi 21,5\" Full HD (1920x1080) IPS ultrafino', 'Pantalla panorámica IPS de 21,5 pulgadas Full HD (1920 x 1080) y tecnología Radeon free Sync. No hay compatibilidad con el montaje VESA Frecuencia de actualización: 75 Hz - Uso del puerto HDMI Diseño de cuadro cero | ultrafino | Tiempo de respuesta de 4 ms | Relación de aspecto del panel IPS: 16: 9. Color admitido: 16. 7 millones de colores. Brillo - 250 nit Ángulo de inclinación -5 grados a 15 grados. Ángulo de visión horizontal: 178 grados. Ángulo de visión vertical: 178 grados 75 hercios', '20220521020304.jpg', '999.00', 'a:1:{i:0;s:7:\"monitor\";}', 8, '2022-05-21 02:03:04', '2022-05-21 02:03:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2022-05-18 03:34:57', '2022-05-18 03:34:57'),
(2, 'Moderador', 'web', '2022-05-18 00:12:30', '2022-05-18 00:12:30'),
(3, 'Cliente', 'web', '2022-05-18 00:12:57', '2022-05-18 00:12:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(6, 3),
(7, 1),
(7, 2),
(7, 3),
(8, 1),
(8, 2),
(8, 3),
(9, 1),
(9, 2),
(10, 1),
(10, 2),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(13, 3),
(14, 1),
(14, 2),
(14, 3),
(15, 1),
(15, 2),
(15, 3),
(16, 1),
(16, 2),
(16, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'Matias Reynolds', 'reymaro@spoanet.com', NULL, '$2y$10$gyxQvFsz4WOT7gZRuNQd2.tvOy42KHHNJ8Z9DbOLexkODFLEM6EaC', 'e0EQm6FACgaiSUD0z8FdM60rXMytnIKH6k7Hqp5YN7lssgRvkjjfS7qmTkKs', '2022-05-18 22:54:41', '2022-05-20 15:25:22'),
(5, 'Moderador', 'moderador@email.com', NULL, '$2y$10$1NHJEiT73YMTGf1UHmUFSeQ6DQEO1XTUbi78G2bmzA9A0HLKPUev6', NULL, '2022-05-21 00:28:36', '2022-05-21 00:28:36'),
(6, 'Administradxr', 'admin@email.com', NULL, '$2y$10$y9udnkXRs3.5PCfvILPjZu8QHvJJKuH/7cJEUczH/abNEom0tbdZy', NULL, '2022-05-21 00:33:14', '2022-05-21 00:33:14'),
(7, 'Billy Blaze', 'billy@email.com', NULL, '$2y$10$xr2.exEBUrLh0/tmU6CpvO0e/sV27XV.XXsXiQxRsVGvBNbGmF.42', NULL, '2022-05-21 01:17:36', '2022-05-21 01:17:36'),
(8, 'Armando Esteban', 'armando@email.com', NULL, '$2y$10$Tsjrlbj3WNuT/5deHcQqWOUbabTzLrzt3SmfCsOo/3Jjf/ZiRYJ46', NULL, '2022-05-21 02:02:23', '2022-05-21 02:02:23');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indices de la tabla `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
