@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="float-left">
            <h2> Ver rol</h2>
        </div>

        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('roles.index') }}"> Atras</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <strong>Nombre:</strong>
            {{ $role->name }}
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <strong>Permisos:</strong>
            @if(!empty($rolePermissions))
                @foreach($rolePermissions as $v)
                    <label class="label label-success">{{ $v->name }},</label>
                @endforeach
            @endif
        </div>
    </div>
</div>
@endsection