@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="float-left">
            <h2>Lista de Roles</h2>
        </div>
        <div class="float-right">
        @can('role-create')
            <a class="btn btn-success" href="{{ route('roles.create') }}"> Crear rol</a>            
        @endcan
        </div>
    </div>
</div>
@if (Session::get('message'))
    <div class="alert alert-{{Session::get('alert')}}">
        {{ Session::get('message') }}
    </div>
@endif
<table class="table table-bordered">
  <tr>
     <th>ID</th>
     <th>Nombre</th>
     <th>Acciones</th>
  </tr>
    @foreach ($roles as $key => $role)
    <tr>
        <td>{{ $role->id }}</td>
        <td>{{ $role->name }}</td>
        <td>
            <a class="btn btn-info" href="{{ route('roles.show',$role->id) }}"><i class="bi bi-eye"></i></a>
            @can('role-edit')
                <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}"><i class="bi bi-pencil-square"></i></a>
            @endcan
            @can('role-delete')
                {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                    {!! Form::button('<i class="bi bi-trash"></i>', ['class' => 'btn btn-danger', 'type' => 'submit']) !!}
                {!! Form::close() !!}
            @endcan
        </td>
    </tr>
    @endforeach
</table>
{!! $roles->render() !!}
@endsection