@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="float-left">
                <h2>Agregar nuevo producto</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Atras</a>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Ups!</strong> Hubo algunos problemas con tus datos ingresados.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {!! Form::open(array('route' => 'products.store','method'=>'POST', 'enctype'=>'multipart/form-data')) !!}
         <div class="row">
		    <div class="col-12">
		        <div class="form-group">
		            <strong>Nombre:</strong>		            
                    {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control')) !!}
		        </div>
		    </div>
		    <div class="col-12">
		        <div class="form-group">
		            <strong>Detalles:</strong>
                    {!! Form::textarea('detail', null, array('placeholder' => 'Detalles','class' => 'form-control')) !!}		            
		        </div>
		    </div>
		    <div class="col-6">
		        <div class="form-group">
		            <strong>Precio:</strong>
                    {!! Form::number('price', null, array('placeholder' => 'Precio','class' => 'form-control')) !!}
		        </div>
		    </div>
		    <div class="col-6">
		        <div class="form-group">
		            <strong>Tags:</strong>
		            {!! Form::select('tags[]', $tags, null, ['class' => 'form-control', 'multiple' => 'multiple']) !!}
		        </div>
		    </div>
		    @if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador'))
		    <div class="col-6">
                <div class="form-group">
                    <strong>Foto:</strong>
                    {!! Form::file('image', null) !!}                    
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <strong>Autor:</strong>
                    {!! Form::select('owner', $users, null, ['class' => 'form-control', 'required' => 'required']) !!}                  
                </div>
            </div>
		    @else
            <div class="col-12">
                <div class="form-group">
                    <strong>Foto:</strong>
                    {!! Form::file('image', null) !!}                    
                </div>
            </div>
            @endif
		    <div class="col-12 text-center">
		            <button type="submit" class="btn btn-primary">Guardar</button>
		    </div>
		</div>
    </form>
@endsection
@section('scripts')
<script>
    $(document).ready( function () {
        $('select').select2({
            maximumSelectionLength: 10,
            tags: true
        });
    });
</script>
@endsection