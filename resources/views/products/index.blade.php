@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="float-left">
                <h2>Productos</h2>
            </div>
            <div class="float-right">
                @can('product-create')
                <a class="btn btn-success" href="{{ route('products.create') }}">Crear Producto</a>
                @endcan
            </div>
        </div>
    </div>
    @if (Session::get('message'))
        <div class="alert alert-{{Session::get('alert')}}">
            {{ Session::get('message') }}
        </div>
    @endif
    <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th>ID</th>            
                <th>Foto</th>
                <th>Nombre</th>
                <th>Detalles</th>
                <th>Tags</th>
                <th>Precio</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($products as $product)
    	    <tr>
    	        <td>{{ $product->id }}</td>
                <td><img src="{{ url('/public/image', $product->image) }}" class="img-thumbnail width-150"></td>
    	        <td>{{ $product->name }}</td>
    	        <td>{{ $product->detail }}</td>
    	        <td>@foreach ($product->tags as $tag) <label class="badge badge-primary">{{ $tag }}</label> @endforeach</td>
                <td>{{ $product->price }}</td>
    	        <td>            
                    <a class="btn btn-sm btn-info" href="{{ route('products.show',$product->id) }}"><i class="bi bi-eye"></i></a>
                    @can('product-edit')
                    <a class="btn btn-sm btn-primary" href="{{ route('products.edit',$product->id) }}"><i class="bi bi-pencil-square"></i></a>
                    @endcan                
                    @can('product-delete')
                        {!! Form::open(['method' => 'DELETE','route' => ['products.destroy', $product->id],'style'=>'display:inline']) !!}
                            {!! Form::button('<i class="bi bi-trash"></i>', ['class' => 'btn btn-sm btn-danger', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    @endcan            
    	        </td>
    	    </tr>
    	    @endforeach
        </tbody>
    </table>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('.table').DataTable({
            responsive: true,
            /*
            language: {
                url: "{{ url('json/datatables/es-ES.json') }}",
            },
            */
        });
    });
</script>
@endsection