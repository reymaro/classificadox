@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="float-left">
            <h2>Ver Producto</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('products.index') }}"> Atras</a>
        </div>
    </div>
</div>
<div class="row text-center">    
    <div class="col-12">
        <div class="form-group">
            <img src="{{ url('/public/image', $product->image) }}" class="img-thumbnail width-500">
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <strong>{{ $product->name }}</strong>
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <strong>$ {{ $product->price }}</strong>
        </div>
    </div>        
    <div class="col-12">
        <div class="form-group">
            {{ $product->detail }}
        </div>
    </div>
</div>
@endsection