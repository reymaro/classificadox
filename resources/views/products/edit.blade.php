@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="float-left">
                <h2>Editar Producto</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-primary" href="{{ route('products.index') }}"> Atras</a>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Ups!</strong> Hubo algunos problemas con tus datos ingresados.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('products.update',$product->id) }}" method="POST" enctype="multipart/form-data">
    	@csrf
        @method('PUT')
         <div class="row">
		    <div class="col-12">
		        <div class="form-group">
		            <strong>Nombre:</strong>
		            <input type="text" name="name" value="{{ $product->name }}" class="form-control" placeholder="Nombre">
		        </div>
		    </div>
		    <div class="col-12">
		        <div class="form-group">
		            <strong>Detalles:</strong>
		            <textarea class="form-control" style="height:150px" name="detail" placeholder="Detalles">{{ $product->detail }}</textarea>
		        </div>
		    </div>
		    <div class="col-6">
		        <div class="form-group">
		            <strong>Precio:</strong>
		            <input type="number" name="price" value="{{ $product->price }}" class="form-control" placeholder="Precio">
		        </div>
		    </div>
		    <div class="col-6">
		        <div class="form-group">
		            <strong>Tags:</strong>
		            <select name="tags[]" class="form-control form-control-select" multiple="multiple">
		                @foreach ($product->tags as $tag)
		                <option value="{{ $tag }}" selected>{{ $tag }}</option>
		                @endforeach
		            </select>
		        </div>
		    </div>  
            @if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador'))
		    <div class="col-6">
                <div class="form-group">
                    <strong>Foto:</strong>
                    {!! Form::file('image', null) !!}                    
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <strong>Autor:</strong>
                    {!! Form::select('owner', $users, $product->owner, ['class' => 'form-control', 'required' => 'required']) !!}                  
                </div>
            </div>
		    @else
            <div class="col-12">
                <div class="form-group">
                    <strong>Foto:</strong>
                    {!! Form::file('image', null) !!}                    
                </div>
            </div>
            @endif
		    <div class="col-12 text-center">
		      <button type="submit" class="btn btn-primary">Guardar</button>
		    </div>
		</div>
    </form>
@endsection
@section('scripts')
<script>
    $(document).ready( function () {
        $('select').select2({
            maximumSelectionLength: 10,
            tags: true
        });
    });
</script>
@endsection