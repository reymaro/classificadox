@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="float-left">
                <h2>Pedidos</h2>
            </div>
            <div class="float-right">
                @can('order-create')
                <a class="btn btn-success" href="{{ route('orders.create') }}">Crear Pedido</a>
                @endcan
            </div>
        </div>
    </div>
    @if (Session::get('message'))
        <div class="alert alert-{{Session::get('alert')}}">
            {{ Session::get('message') }}
        </div>
    @endif
    <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th>ID</th>            
                <th>Comprador</th>
                <th>Vendedor</th>
                <th>Monto</th>
                <th>Estado</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $order)
    	    <tr>
    	        <td>{{ $order->id }}</td>
    	        <td>{{ $order->from }}</td>
    	        <td>{{ $order->to }}</td>
                <td>$ {{ $order->content['amount'] }}</td>
                <td>{{ $order->status }}</td>
    	        <td>
    	            @php
    	            $class = ($order->status == 'pendiente') ? 'btn btn-sm btn-warning' : 'btn btn-sm btn-success';
    	            $icon = ($order->status == 'pendiente') ? '<i class="bi bi-truck"></i>' : '<i class="bi bi-box2-heart-fill"></i>';
    	            @endphp
    	            @if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador') || Auth::user()->name == $order->from || Auth::user()->name == $order->to)
    	                {!! Form::open(['method' => 'PUT','route' => ['orders.update',$order->id],'style'=>'display:inline']) !!}
                            {!! Form::button($icon, ['class' => $class, 'type' => 'submit']) !!}
                        {!! Form::close() !!}
    	            @endif
                    <a class="btn btn-sm btn-info" href="{{ route('orders.show',$order->id) }}"><i class="bi bi-eye"></i></a>
                    @can('product-edit')
                    <a class="btn btn-sm btn-primary" href="{{ route('orders.edit',$order->id) }}"><i class="bi bi-pencil-square"></i></a>
                    @endcan                
                    @can('product-delete')
                        {!! Form::open(['method' => 'DELETE','route' => ['orders.destroy', $order->id],'style'=>'display:inline']) !!}
                            {!! Form::button('<i class="bi bi-trash"></i>', ['class' => 'btn btn-sm btn-danger', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    @endcan            
    	        </td>
    	    </tr>
    	    @endforeach
        </tbody>
    </table>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('.table').DataTable({
            responsive: true,
            /*
            language: {
                url: "{{ url('json/datatables/es-ES.json') }}",
            },
            */
        });
    });
</script>
@endsection