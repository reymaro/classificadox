@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="float-left">
            <h2>Ver Pedido</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('orders.index') }}"> Atras</a>
        </div>
    </div>
</div>
<div class="row">    
    <div class="col-2">
    </div>
    <div class="col-8">
        <div class="row">
            <div class="col-12 mb-10 border border-secondary">
                <p class="float-right">Fecha: {{ $order->created_at }}</p>
            </div>
            <div class="col-6 border border-secondary">
                <div class="float-left">
                    <p><strong>Datos del Comprador</strong></p>
                    <p>Nombre: {{ $order->from }}</p>
                </div>
            </div>
            <div class="col-6 border border-secondary">
                <div class="float-left">
                    <p><strong>Datos del Vendedor</strong></p>
                    <p>Nombre: {{ $order->to }}</p>
                </div>
            </div>
            <div class="col-12 border border-secondary p-0 m-0">
                <table class="table table-striped table-bordered p-0 m-0">
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Cantidad</th>
                            <th>Precio</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $order->content['product_name'] }}</td>
                            <td>{{ $order->content['quantity'] }}</td>
                            <td>$ {{ $order->content['amount'] / $order->content['quantity'] }}</td>
                            <td>$ {{ $order->content['amount'] }}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3"></td>
                            <td><strong>Total:</strong> $ {{ $order->content['amount'] }}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="col-2">
    </div>
</div>
@endsection