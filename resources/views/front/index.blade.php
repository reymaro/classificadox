<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<title>{{ config('app.name') }}</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{ url('public/front/images/icons/favicon.png') }}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('public/front/vendor/bootstrap/css/bootstrap.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('public/front/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('public/front/fonts/iconic/css/material-design-iconic-font.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('public/front/fonts/linearicons-v1.0.0/icon-font.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('public/front/vendor/animate/animate.css') }}">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ url('public/front/vendor/css-hamburgers/hamburgers.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('public/front/vendor/animsition/css/animsition.min.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('public/front/vendor/slick/slick.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('public/front/vendor/MagnificPopup/magnific-popup.css') }}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('public/front/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('public/front/css/main.css') }}">
<!--===============================================================================================-->
</head>
<body class="animsition">
@csrf
	<header>
		<div class="container-menu-desktop">
			<div class="wrap-menu-desktop">
				<nav class="navbar limiter-menu-desktop container">
					<a href="{{ url('/') }}" class="logo">
						<img src="{{ url('public/front/images/classificadox.png') }}" alt="IMG-LOGO">
					</a>
					<div class="menu-desktop">
						<ul class="main-menu">
                        @if (Route::has('login'))
                            @auth
                            <li><a href="{{ url('/dashboard') }}">Mi Cuenta</a></li>
                            @else
                             <li><a href="{{ route('login') }}">Ingresar</a></li>
                                @if (Route::has('register'))
                                <li><a href="{{ route('register') }}">Registro</a></li>
                                @endif
                            @endif
                        @endif
						</ul>
					</div>	
				</nav>
			</div>	
		</div>
		<div class="wrap-header-mobile">
			<div class="logo-mobile">
				<a href="{{ url('/') }}"><img src="{{ url('public/front/images/classificadox.png') }}" alt="IMG-LOGO"></a>
			</div>
			<div class="btn-show-menu-mobile hamburger hamburger--squeeze">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</div>
		</div>
		<div class="menu-mobile">
			<ul class="main-menu-m">
			@if (Route::has('login'))
                @auth
                <li><a href="{{ url('/dashboard') }}">Mi Cuenta</a></li>
                @else
                 <li><a href="{{ route('login') }}">Ingresar</a></li>
                    @if (Route::has('register'))
                    <li><a href="{{ route('register') }}">Registro</a></li>
                    @endif
                @endif
            @endif
			</ul>
		</div>
	</header>
	<section class="section-slide">
		<div class="wrap-slick1">
			<div class="slick1">
				<div class="item-slick1" style="background-image: url({{ url('public/front/images/slide-01.jpg') }});">
					<div class="container h-full">
						<div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
							<div class="layer-slick1 animated visible-false" data-appear="fadeInDown" data-delay="0">
								<span class="ltext-101 cl2 respon2">
									El sitio más simple donde ofertar tu producto.
								</span>
							</div>
								
							<div class="layer-slick1 animated visible-false" data-appear="fadeInUp" data-delay="800">
								<h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1">
									¡Bienvenidx a ClassificadoX!
								</h2>
							</div>
						</div>
					</div>
				</div>
				<div class="item-slick1" style="background-image: url({{ url('public/front/images/slide-02.jpg') }});">
					<div class="container h-full">
						<div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
							<div class="layer-slick1 animated visible-false" data-appear="rollIn" data-delay="0">
								<span class="ltext-101 cl2 respon2">
									Inicia sesión o regístrate y publica productos o contacta vendedores.
								</span>
							</div>
								
							<div class="layer-slick1 animated visible-false" data-appear="lightSpeedIn" data-delay="800">
								<h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1">
									¡Es gratis!
								</h2>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>
	<section class="bg0 p-t-23 p-b-140">
		<div class="container">
			<div class="flex-w flex-sb-m p-b-52">
				<div class="flex-w flex-l-m filter-tope-group m-tb-10">
					<button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5 how-active1" data-filter="*">
						Todos los Productos
					</button>
					@if (count($tags) > 0)
					@foreach ($tags as $tag)
					<button class="stext-106 cl6 hov1 bor3 trans-04 m-r-32 m-tb-5" data-filter=".{{ $tag }}">
						{{ $tag }}
					</button>
					@endforeach
					@endif
				</div>
			</div>
			<div class="row isotope-grid">
			    @if (count($products) > 0)
			    @foreach ($products as $product)
				<div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item @foreach ($product->tags as $tag) {{ $tag }} @endforeach">
					<div class="block2">
						<div class="block2-pic hov-img0">
							<img src="{{ url('/public/image', $product->image) }}" alt="IMG-PRODUCT">
							@guest
							<a href="{{ url('login') }}" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04">
								Iniciar Sesión
							</a>
							@else
							<a href="javascript:void(0);" class="block2-btn flex-c-m stext-103 cl2 size-102 bg0 bor2 hov-btn1 p-lr-15 trans-04 js-show-modal1" data-product_id="{{ $product->id }}">
								Ver
							</a>
							@endguest
						</div>
						<div class="block2-txt flex-w flex-t p-t-14">
							<div class="block2-txt-child1 flex-col-l ">
								<a href="javascript:void(0);" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
									{{ $product->name }}
								</a>
								<span class="stext-105 cl3">
									$ {{ $product->price }}
								</span>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				@endif
			</div>
		</div>
	</section>
	<footer class="bg3 p-b-10">
		<div class="container">
			<div class="p-t-10">
				<p class="stext-107 cl6 txt-center">
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Hecho con <i class="fa fa-heart-o" aria-hidden="true"></i> by <strong>ClassificadoX</strong>
				</p>
			</div>
		</div>
	</footer>
	<div class="btn-back-to-top" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="zmdi zmdi-chevron-up"></i>
		</span>
	</div>

	<div class="wrap-modal1 js-modal1 p-t-60 p-b-20">
		<div class="overlay-modal1 js-hide-modal1"></div>
		<div class="container">
			<div class="bg0 p-t-60 p-b-30 p-lr-15-lg how-pos3-parent">
				<button class="how-pos3 hov3 trans-04 js-hide-modal1">
					<img src="{{ url('public/front/images/icons/icon-close.png') }}" alt="CLOSE">
				</button>
				<div class="row">
					<div class="col-md-6 col-lg-8 p-b-30">
						<div class="p-l-25 p-r-30 p-lr-0-lg">
							<div class="wrap-slick3 flex-sb flex-w">
								<div class="slick3 gallery-lb m-auto">
									<div id="product_image_thumb" class="item-slick3" data-thumb="{{ url('public/front/images/product-detail-01.jpg') }}">
										<div class="wrap-pic-w pos-relative">
											<img id="product_image" src="" alt="IMG-PRODUCT">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-md-6 col-lg-4 p-b-30">
						<div class="p-r-50 p-t-5 p-lr-0-lg">
						    <p id="product_id" class="d-none"></p>
						    <p id="product_owner" class="d-none"></p>
							<h4 id="product_name" class="mtext-105 cl2 js-name-detail p-b-14">
								
							</h4>

							<span id="product_price" class="mtext-106 cl2">
								
							</span>

							<p id="product_detail" class="stext-102 cl3 p-t-23">
								
							</p>
							<div class="p-t-33">
                                <div class="flex-w p-b-10">
									<div class="size-204 flex-w flex-m respon6-next">
										<div class="wrap-num-product flex-w m-r-20 m-tb-10">
											<div class="btn-num-product-down cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-minus"></i>
											</div>
											<input class="mtext-104 cl3 txt-center num-product" type="number" id="quantity" value="1">
											<div class="btn-num-product-up cl8 hov-btn3 trans-04 flex-c-m">
												<i class="fs-16 zmdi zmdi-plus"></i>
											</div>
										</div>
                                        <div class="flex-w m-r-20 m-tb-10">
                                            <textarea id="message" class="stext-111 cl2 plh3 bor8 p-lr-28 p-tb-25"></textarea>
                                        </div>
										<button class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04 js-addcart-detail">
											Enviar Pedido
										</button>
									</div>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-search-header flex-c-m trans-04 js-hide-modal-search">
		<div class="container-search-header">
			<button class="flex-c-m btn-hide-modal-search trans-04 js-hide-modal-search">
				<img src="{{ url('public/front/images/icons/icon-close2.png') }}" alt="CLOSE">
			</button>
			
			<div class="response w-100">
			    
			</div>
		</div>
	</div>
	
	<script src="{{ url('public/front/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ url('public/front/vendor/animsition/js/animsition.min.js') }}"></script>
	<script src="{{ url('public/front/vendor/bootstrap/js/popper.js') }}"></script>
	<script src="{{ url('public/front/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ url('public/front/vendor/slick/slick.min.js') }}"></script>
	<script src="{{ url('public/front/js/slick-custom.js') }}"></script>
	<script src="{{ url('public/front/vendor/MagnificPopup/jquery.magnific-popup.min.js') }}"></script>
	<script>
		$('.gallery-lb').each(function() { // the containers for all your galleries
			$(this).magnificPopup({
		        delegate: 'a', // the selector for gallery item
		        type: 'image',
		        gallery: {
		        	enabled:true
		        },
		        mainClass: 'mfp-fade'
		    });
		});
	</script>
	<script src="{{ url('public/front/vendor/isotope/isotope.pkgd.min.js') }}"></script>
	<!--<script src="{{ url('public/front/js/main.js') }}"></script>-->
	
	<script>
	    /*[ Load page ] ===========================================================*/
        $(".animsition").animsition({
            inClass: 'fade-in',
            outClass: 'fade-out',
            inDuration: 1500,
            outDuration: 800,
            linkElement: '.animsition-link',
            loading: true,
            loadingParentElement: 'html',
            loadingClass: 'animsition-loading-1',
            loadingInner: '<div class="loader05"></div>',
            timeout: false,
            timeoutCountdown: 5000,
            onLoadEvent: true,
            browser: [ 'animation-duration', '-webkit-animation-duration'],
            overlay : false,
            overlayClass : 'animsition-overlay-slide',
            overlayParentElement : 'html',
            transition: function(url){ window.location.href = url; }
        });
        
        /*[ Back to top ] ===========================================================*/
        var windowH = $(window).height()/2;
    
        $(window).on('scroll',function(){
            if ($(this).scrollTop() > windowH) {
                $("#myBtn").css('display','flex');
            } else {
                $("#myBtn").css('display','none');
            }
        });
    
        $('#myBtn').on("click", function(){
            $('html, body').animate({scrollTop: 0}, 300);
        });
    
        /*================================================================== [ Fixed Header ]*/
        var headerDesktop = $('.container-menu-desktop');
        var wrapMenu = $('.wrap-menu-desktop');
        
        if($('.top-bar').length > 0) {
            var posWrapHeader = $('.top-bar').height();
        } else {
            var posWrapHeader = 0;
        }
        
        if($(window).scrollTop() > posWrapHeader) {
            $(headerDesktop).addClass('fix-menu-desktop');
            $(wrapMenu).css('top',0); 
        } else {
            $(headerDesktop).removeClass('fix-menu-desktop');
            $(wrapMenu).css('top',posWrapHeader - $(this).scrollTop()); 
        }
        
        $(window).on('scroll',function(){
            if($(this).scrollTop() > posWrapHeader) {
                $(headerDesktop).addClass('fix-menu-desktop');
                $(wrapMenu).css('top',0); 
            } else {
                $(headerDesktop).removeClass('fix-menu-desktop');
                $(wrapMenu).css('top',posWrapHeader - $(this).scrollTop()); 
            } 
        });
        
        /*================================================================== [ Menu mobile ]*/
        $('.btn-show-menu-mobile').on('click', function(){
            $(this).toggleClass('is-active');
            $('.menu-mobile').slideToggle();
        });
    
        var arrowMainMenu = $('.arrow-main-menu-m');
    
        for(var i=0; i<arrowMainMenu.length; i++){
            $(arrowMainMenu[i]).on('click', function(){
                $(this).parent().find('.sub-menu-m').slideToggle();
                $(this).toggleClass('turn-arrow-main-menu-m');
            })
        }
    
        $(window).resize(function(){
            if($(window).width() >= 992){
                if($('.menu-mobile').css('display') == 'block') {
                    $('.menu-mobile').css('display','none');
                    $('.btn-show-menu-mobile').toggleClass('is-active');
                }
    
                $('.sub-menu-m').each(function(){
                    if($(this).css('display') == 'block') { console.log('hello');
                        $(this).css('display','none');
                        $(arrowMainMenu).removeClass('turn-arrow-main-menu-m');
                    }
                });
                    
            }
        });
        
        /*================================================================== [ Isotope ]*/
        var $topeContainer = $('.isotope-grid');
        var $filter = $('.filter-tope-group');
    
        // filter items on button click
        $filter.each(function () {
            $filter.on('click', 'button', function () {
                var filterValue = $(this).attr('data-filter');
                $topeContainer.isotope({filter: filterValue});
            });
            
        });
    
        // init Isotope
        $(window).on('load', function () {
            var $grid = $topeContainer.each(function () {
                $(this).isotope({
                    itemSelector: '.isotope-item',
                    layoutMode: 'fitRows',
                    percentPosition: true,
                    animationEngine : 'best-available',
                    masonry: {
                        columnWidth: '.isotope-item'
                    }
                });
            });
        });
    
        var isotopeButton = $('.filter-tope-group button');
    
        $(isotopeButton).each(function(){
            $(this).on('click', function(){
                for(var i=0; i<isotopeButton.length; i++) {
                    $(isotopeButton[i]).removeClass('how-active1');
                }
    
                $(this).addClass('how-active1');
            });
        });
        
        /*================================================================== [ +/- num product ]*/
        $('.btn-num-product-down').on('click', function(){
            var numProduct = Number($(this).next().val());
            if(numProduct > 0) $(this).next().val(numProduct - 1);
        });
    
        $('.btn-num-product-up').on('click', function(){
            var numProduct = Number($(this).prev().val());
            $(this).prev().val(numProduct + 1);
        });
        
        /*================================================================== [ Show modal1 ]*/
        @guest
        
        @else
        $('.js-show-modal1').on('click',function(e){
            e.preventDefault();
            
            $.get("{{ url('/products/get') }}"+"/"+$(this).attr('data-product_id'))
            .done(function( response ) {
                var data = JSON.parse(response);
                var image_path = "{{ url('public/image') }}";
                
                $('#product_id').text(data.id);
                $('#product_owner').text(data.owner);
                $('#product_name').text(data.name);
                $('#product_price').text(data.price);
                $('#product_detail').text(data.detail);
                $('#product_image').attr('src', image_path + '/' + data.image);
            });
            
            $('.js-modal1').addClass('show-modal1');
        });
    
        $('.js-hide-modal1').on('click',function(){
            $('.js-modal1').removeClass('show-modal1');
        });
        
        $('.js-addcart-detail').on('click',function(e){
            e.preventDefault();
            
            $.post("{{ url('/orders/store') }}", { 
                _token: $("input[name=_token").val(),
                from: {{ Auth::user()->id }},
                to: $('#product_owner').text(),
                product: $('#product_id').text(),
                quantity: $('#quantity').val(),
                message: $('#message').val()
            }).done(function( response ) {
                var data = JSON.parse(response);
                
                $('.response').parent().parent().addClass('show-modal-search');
                
                $('.response').html(`
                    <p>Muy bien! Tu pedido fue enviado al Vendedor, aquí los datos:<p>
                    <p><strong>Producto:</strong> `+data.content['product_name']+`</p>
                    <p><strong>Cantidad:</strong> `+data.content['quantity']+` | <strong>Monto:</strong> $ `+data.content['amount']+`</p>
                    <p><strong>Vendedor:</strong> `+data.to+`</p>
                    <p><strong>Comprador:</strong> `+data.from+`</p>
                    <p><strong>Fecha:</strong> `+data.created_at+`</p>
                `);
                
                $('.js-modal1').removeClass('show-modal1');
            });
        });
        
        $('.js-hide-modal-search').on('click', function(){
            $('.modal-search-header').removeClass('show-modal-search');
            $('.js-show-modal-search').css('opacity','1');
        });
        @endguest
	</script>
</body>
</html>