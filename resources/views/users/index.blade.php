@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="float-left">
            <h2>Lista de Usuarios</h2>
        </div>
        <div class="float-right">
          @can('user-create')
            <a class="btn btn-success" href="{{ route('users.create') }}"> Crear Usuario</a>
          @endcan
        </div>
    </div>
</div>
@if (Session::get('message'))
    <div class="alert alert-{{Session::get('alert')}}">
        {{ Session::get('message') }}
    </div>
@endif
<table class="table table-sm table-bordered">
 <thead>
     <tr>
       <th>ID</th>
       <th>Nombre</th>
       <th>Email</th>
       <th>Roles</th>
       <th>Accion</th>
     </tr>
 </thead>
 <tbody>
 @foreach ($data as $key => $user)
  <tr>
    <td>{{ $user->id }}</td>
    <td>{{ $user->name }}</td>
    <td>{{ $user->email }}</td>
    <td>
      @if(!empty($user->getRoleNames()))
        @foreach($user->getRoleNames() as $v)
           <label class="badge badge-success">{{ $v }}</label>
        @endforeach
      @endif
    </td>

    <td>
       <a class="btn btn-sm btn-info" href="{{ route('users.show',$user->id) }}"><i class="bi bi-eye"></i></a>
      @can('user-edit')
       <a class="btn btn-sm btn-primary" href="{{ route('users.edit',$user->id) }}"><i class="bi bi-pencil-square"></i></a>
      @endcan
      @can('user-delete')
        {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
            {!! Form::button('<i class="bi bi-trash"></i>', ['class' => 'btn btn-sm btn-danger', 'type' => 'submit']) !!}
        {!! Form::close() !!}
      @endcan
    </td>
  </tr>
  </tbody>
 @endforeach
</table>
{!! $data->render() !!}
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        $('.table').DataTable({
            responsive: true,
        });
    });
</script>
@endsection