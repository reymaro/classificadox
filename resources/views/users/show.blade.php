@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-12">
        <div class="float-left">
            <h2>Perfil de Usuario</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-primary" href="{{ route('users.index') }}"> Atras</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="form-group">
            <strong>Nombre:</strong>
            {{ $user->name }}
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <strong>E-mail:</strong>
            {{ $user->email }}
        </div>
    </div>

    <div class="col-12">
        <div class="form-group">
            <strong>Roles:</strong>
            @if(!empty($user->getRoleNames()))
                @foreach($user->getRoleNames() as $v)
                    <label class="badge badge-success">{{ $v }}</label>
                @endforeach
            @endif
        </div>
    </div>

</div>
@endsection