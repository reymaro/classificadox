@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Verificar tu direccion de E-Mail</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.
                        </div>
                    @endif
                    Antes de continuar, verifique en su correo electrónico que no haya recibido un enlace de verificación.<br>
                    Si no lo ha recibido aun,
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">haga click aqui para solicitar otro.</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
