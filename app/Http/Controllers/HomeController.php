<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\Orders;

use Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $products = Product::where('owner', Auth::user()->id)->get();
        $orders = Orders::where('from', Auth::user()->id)->orWhere('to', Auth::user()->id)->get();
        
        return view('dashboard', compact(['products', 'orders']));
    }
}
