<?php 
namespace App\Http\Controllers;  

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;

class ProductController extends Controller
{
    // Seteamos permisos regun el rol, los usuarios tienen acceso a las diferentes funciones.
    function __construct()
    {
         $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index','show', 'get']]);
         $this->middleware('permission:product-create', ['only' => ['create','store']]);
         $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }
    
    /*
     * La funcion index() discrimina entre si el usuario es un Administrador/Moderador o un Cliente.
     * Para cada caso muestra los productos correspondientes, todos para admin/mod y solo aquellos de quien el cliente es propietario.
     * Utilizo serialize() / unserialize() (si, como wordpress) para guardar un array PHP con tags que luego me sirven para filtrar.
     */
    public function index()
    {
        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador')) {
            $products = Product::all();
        } else {
            $products = Product::where('owner', Auth::user()->id)->get();
        }
        
        foreach ($products as $product) {
            $product->tags = unserialize($product->tags);
        }
        
        return view('products.index',compact('products'));
    }   
    
    /*
     * La funcion create() muestra el formulario para crear un producto, no hay mayores aclaraciones.
     */
    public function create()
    {
        $users_query = User::all();
        $users = array();
        
        foreach ($users_query as $user) {
            $users[$user->id] = $user->name.' ('.$user->email.')';
        }
        
        $products = Product::all();
        $tags = [];
        
        foreach ($products as $product) {
            $product->tags = unserialize($product->tags);
            
            foreach ($product->tags as $tag) {
                if (!in_array($tag, $tags)) $tags[$tag] = $tag;
            }
            
        }
        
        return view('products.create',compact('tags', 'users'));
    }
    
    /*
     * La funcion store() guarda una entrada en la Base de Datos luego de hacer ciertas validaciones.
     * Se puede apreciar el manejo de imagenes, que son subidas a la carpeta image/ en el directorio publico de Laravel.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
            'price'=>'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg,webp|max:2048',
            'tags' => 'required',
        ]); 

        $input = $request->all();  

        if ($image = $request->file('image')) {
            $destinationPath = 'image/';
            $productImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
            $image->move($destinationPath, $productImage);
            $input['image'] = "$productImage";
        }
        
        $input['tags'] = serialize($input['tags']);
        $input['owner'] = ($request->owner) ? $request->owner : Auth::user()->id;
        $input['status'] = 'revision';

        Product::create($input);        

        return redirect()->route('products.index')->with('alert', 'success')->with('message','Producto creado correctamente');
    }
    
    /*
     * La funcion show() muestra una vista con los datos del producto sin mayores aclaraciones.
     */
    public function show(Product $product)
    {
        $product->tags = unserialize($product->tags);
            
        return view('products.show',compact('product'));
    }
    
    /*
     * La funcion edit() muestra el formulario para editar un producto.
     * Se discrimina entre usuarios admin/mod y clientes, pues estos ultimos solo pueden editar productos donde son propietarios.
     */
    public function edit(Product $product)
    {
        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador') || $product->owner == Auth::user()->id) {
            $users_query = User::all();
            $users = array();
            
            foreach ($users_query as $user) {
                $users[$user->id] = $user->name.' ('.$user->email.')';
            }
            
            $product->tags = unserialize($product->tags);
                
            return view('products.edit',compact('product', 'users'));
        } else {
            return redirect()->back()->with('alert', 'danger')->with('message','No tiene permisos para editar este producto.');
        }
    }  
    
    /*
     * La funcion update() actualiza la entrada en la BD sin mayores aclaraciones pues es similiar a store().
     */
    public function update(Request $request, Product $product)
    {
        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador') || $product->owner == Auth::user()->id) {
            $request->validate([
                'name' => 'required',
                'detail' => 'required',
                'price'=>'required',
                'tags' => 'required',
            ]);
            
            $input = $request->all();  
            
            if ($image = $request->file('image')) {
                $destinationPath = 'image/';
                $productImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $productImage);
                $input['image'] = "$productImage";
            }else{
                unset($input['image']);
            }          
            
            $input['tags'] = serialize($input['tags']);
            $input['owner'] = ($request->owner) ? $request->owner : Auth::user()->id;
            
            $product->update($input);
            
            return redirect()->route('products.index')->with('alert', 'success')->with('message','Producto actualizado correctamente');
        } else {
            return redirect()->back()->with('alert', 'danger')->with('message','No tiene permisos para actualizar este producto.');
        }
    }
    
    /*
     * La funcion destroy() elimina la entrada en la BD y borra el archivo asociado.
     */
    public function destroy(Product $product)
    {
        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador') || $product->owner == Auth::user()->id) {
            $ImagePath = 'image/'.$product->image;
            
            unset($ImagePath);
            
            $product->delete();
            
            return redirect()->route('products.index')->with('alert', 'success')->with('message','Producto eliminado');
        } else {
            return redirect()->back()->with('alert', 'danger')->with('message','No tiene permisos para eliminar este producto.');
        }
    }
    
    /*
     * La funcion get() con metodo GET es la que llamamos desde el Frontend mediante Ajax, devuelve el resultado codificado en JSON con json_encode()
     */
    public function get($product_id)
    {
        $product = Product::where('id', $product_id)->first();
        $product->tags = unserialize($product->tags);
        
        return json_encode($product->toArray());
    }
}