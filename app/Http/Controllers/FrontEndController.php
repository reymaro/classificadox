<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;
use App\Models\User;
use Spatie\Permission\Models\Role;

use Auth;

class FrontEndController extends Controller
{
    function frontend()
    {
        $products = Product::all();
        $tags = [];
        
        foreach ($products as $product) {
            $product->tags = unserialize($product->tags);
            
            foreach ($product->tags as $tag) {
                if (!in_array($tag, $tags)) $tags[$tag] = $tag;
            }
            
        }
        
        return view('front.index',compact(['tags', 'products']));
    }
}
