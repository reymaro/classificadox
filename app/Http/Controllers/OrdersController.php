<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Orders;
use App\Models\Product;
use App\Models\User;

use Auth;
use DB;
use Carbon\Carbon;

class OrdersController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:order-list|order-create|order-edit|order-delete', ['only' => ['index','show']]);
         $this->middleware('permission:order-create', ['only' => ['create','store']]);
         $this->middleware('permission:order-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:order-delete', ['only' => ['destroy']]);
    }
    
    public function index()
    {
        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador')) {
            $orders = Orders::all();
        } else {
            $orders = Orders::where('from', Auth::user()->id)->orWhere('to', Auth::user()->id)->get();
        }
        
        foreach ($orders as $order) {
            $order->content = unserialize($order->content);
            $order->from = $this->get_user_data($order->from, 'name');
            $order->to = $this->get_user_data($order->to, 'name');
        
        }
        
        return view('orders.index',compact('orders'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $product = Product::where('id', $request->product)->first();
        $buyer = User::where('id', $request->from)->first();
        $seller = User::where('id', $request->to)->first();
        $order = array();
        
        $order['from'] = $request->from;
        $order['to'] = $request->to;
        
        $content = [
            'product_id' => $request->product,
            'product_name' => $product->name,
            'quantity' => $request->quantity,
            'message' => $request->message,
            'amount' => $request->quantity * $product->price
        ];
        
        $order['content'] = serialize($content);
        $order['status'] = 'pendiente';
        $order['created_at'] = Carbon::now();
        
        $create_order = Orders::insertGetId($order);
        $get_new_order = Orders::where('id', $create_order)->first();
        
        $get_new_order->content = unserialize($get_new_order->content);
        $get_new_order->from = $this->get_user_data($get_new_order->from, 'name') . '(' . $this->get_user_data($get_new_order->from, 'email') . ')';
        $get_new_order->to = $this->get_user_data($get_new_order->to, 'name') . '(' . $this->get_user_data($get_new_order->to, 'email') . ')';
        
        return json_encode($get_new_order->toArray());
    }

    public function show(Orders $orders, $id)
    {
        $order = Orders::where('id', $id)->first();
        
        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador') || $order->from == Auth::user()->id || $order->to == Auth::user()->id) {
            $order->content = unserialize($order->content);
            $order->from = $this->get_user_data($order->from, 'name');
            $order->to = $this->get_user_data($order->to, 'name');
            
            return view('orders.show',compact('order'));
        } else {
            return redirect()->back()->with('alert', 'danger')->with('message','No tiene permisos para ver este pedido.');
        }
    }

    public function edit(Orders $orders)
    {
        //
    }

    public function update(Orders $orders, $id)
    {
        $order = Orders::where('id', $id)->first();
        
        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador') || $order->from == Auth::user()->id || $order->to == Auth::user()->id) {
            
            $switch = ($order->status == 'pendiente') ? 'entregado' : 'pendiente';
            
            $update = Orders::where('id', $order->id)->update(['status' => $switch]);
            
            return redirect()->back()->with('alert', 'success')->with('message','Pedido actualizado.');
        } else {
            return redirect()->back()->with('alert', 'danger')->with('message','No tiene permisos para actualizar este pedido.');
        }
    }

    public function destroy(Orders $orders, $id)
    {
        $order = Orders::where('id', $id)->first();
        
        if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador') || $order->from == Auth::user()->id || $order->to == Auth::user()->id) {
            $order->delete();
            
            return redirect()->back()->with('alert', 'success')->with('message','Pedido eliminado.');
        } else {
            return redirect()->back()->with('alert', 'danger')->with('message','No tiene permisos para eliminar este pedido.');
        }
    }
    
    /* HELPERS */
    public function get_user_data($user_id, $field = NULL)
    {
        $user = User::where('id', $user_id)->first();
        
        if (!is_null($user)) {
            if ($field) {
                return $user->$field;
            } else {
                return $user;
            }
        } else {
            return 'Desconocido';
        }
    }
}
