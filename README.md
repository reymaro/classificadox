<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="200"></a><a href="https://jarvis.spoanet.com/classificadox/" target="_blank"><img src="https://jarvis.spoanet.com/classificadox/public/front/images/classificadox.png" width="200"></a></p>

# Laravel 8 - ClassificadoX
## Pre-requisitos
```
Servidor Web LAMP/LEMP/WAMP/XAMPP
Laravel => 8
PHP => 7
MariaDB / MySQL => 10 / 5
```
## Instalando este proyecto
Como utilizamos Laravel 8, trabajaremos con consola de comandos.
Luego de descomprimir el archivo **matias_reynolds.zip**, en la raíz del proyecto se encuentra el archivo **matias_reynolds.sql** para importar en MySQL.

Una vez importada la base de datos, configurar algunos datos como los de conexion y el root del proyecto en el archivo **.env**
```
APP_URL=http://localhost/classificadox

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=matias_reynolds
DB_USERNAME=root
DB_PASSWORD=
```
Algunos comandos para asegurar el correcto funcionamiento (en chown utilizar el usuario y grupo correspondiente al servidor web), entonces dentro de la raíz del proyecto:
```
sudo chown -R www:www *
sudo php artisan key:generate
sudo php artisan optimize:clear
```
En el navegador http://localhost/classificadox vamos a **Ingresar** >> cuenta admin por defecto:
```
usuario: admin@email.com
contraseña: admin
```
## Video con las funcionalidades
[Link al Video](https://jarvis.spoanet.com/classificadox/classificadox.webm "Link al Video")

## El sistema en sí.
Como dijimos que vamos a utilizar Laravel, vamos a trabajar bajo las reglas y estructura del propio framework, esto quiere decir que vamos a tener cuatro áreas de trabajo bien marcadas:
> 1. Rutas, principalmente en el archivo **/routes/web.php**
> 2. Modelos y Controladores
	Dentro del directorio **app/Models** y en **app/Http/Controllers**
> 3. Vistas (utilizando Blade, el gestor de plantillas HTML incluído en Laravel)
	Dentro del directorio **/resources/views**
> 4. Directorio para archivos públicos, plugins, hojas de estilo (css), javascript, imagenes, etc.
	Dentro del directorio **/public**

### Archivo **/routes/web.php**
    <?php
	// Cargamos los contrladores de nuestro proyecto de esta forma para enviar los métodos automáticamente al controlador (index, show, edit, etc) (lo voy a identificar como forma compacta)
    use Illuminate\Support\Facades\Route;
    use App\Http\Controllers\HomeController;
    use App\Http\Controllers\RoleController;
    use App\Http\Controllers\UserController;
    use App\Http\Controllers\ProductController;
    use App\Http\Controllers\FrontEndController;
    use App\Http\Controllers\OrdersController;
	
    //Route::get('/', function () { return view('welcome'); });
    Route::get('/', [FrontEndController::class, 'frontend']);
	
    Auth::routes();
	
    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
	
    Route::group(['middleware' => ['auth']], function() {
        Route::resource('roles', RoleController::class); // compacta usando resource
        Route::resource('users', UserController::class);
        Route::resource('products', ProductController::class);
        Route::resource('orders', OrdersController::class);
		
        Route::post('/orders/store', [OrdersController::class, 'store']);  // usando metodo
        Route::get('/products/get/{product_id}', [ProductController::class, 'get']); // ajax!
    });
    

### Archivo **/app/Models/User.php**
Modelo para el controlador de Usuarios, se conecta con la base de datos y hace comprobaciones (notificaciones, roles, login, etc), elegí uno para comentar, son similares todos.

    <?php
    namespace App\Models;
    use Illuminate\Contracts\Auth\MustVerifyEmail;
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Notifications\Notifiable;
    use Spatie\Permission\Traits\HasRoles;
	
    class User extends Authenticatable
    {
        use HasFactory, Notifiable, HasRoles;
		
        protected $fillable = [ // campos que pueden ser asignables en masa
            'name',
            'email',
            'password',
        ];
		
        protected $hidden = [ // campos que se mostraran "ocultos" en los objetos devueltos por laravel
            'password',
            'remember_token',
        ];
		
        protected $casts = [ // para un cambio mas eficiente en el tipo de datos que puede recibir estos campos
            'email_verified_at' => 'datetime',
        ];
    }

### Archivo **/app/HttpController/ProductController.php**
Controlador para la seccion Productos, este controlador es el más completo asi que lo puse de ejemplo.

    <?php 
    namespace App\Http\Controllers;
	use Illuminate\Http\Request;
		
    use App\Models\Product; // Cargamos modelos para conexion eloquente a BD.
    use App\Models\User;
	
    use Auth; // Esta clase nos sirve para obtener informacion relacionada con el usuario logueado al sistema
	
    class ProductController extends Controller
    {
        // Permisos regun el rol, los usuarios tienen acceso a las diferentes funciones.
        function __construct()
        {
             $this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index','show', 'get']]);
             $this->middleware('permission:product-create', ['only' => ['create','store']]);
             $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
             $this->middleware('permission:product-delete', ['only' => ['destroy']]);
        }
        
        /*
         * La funcion index() discrimina entre si el usuario es un Administrador/Moderador o un Cliente.
         * Para cada caso muestra los productos correspondientes, todos para admin/mod y solo aquellos de quien el cliente es propietario.
         * Utilizo serialize() / unserialize() (si, como wordpress) para guardar un array PHP con tags que luego me sirven para filtrar.
         */
        public function index()
        {
            if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador')) {
                $products = Product::all();
            } else {
                $products = Product::where('owner', Auth::user()->id)->get();
            }
            
            foreach ($products as $product) {
                $product->tags = unserialize($product->tags);
            }
            
            return view('products.index',compact('products'));
        }   
        
        /*
         * La funcion create() muestra el formulario para crear un producto, no hay mayores aclaraciones.
         */
        public function create()
        {
            $users_query = User::all();
            $users = array();
            
            foreach ($users_query as $user) {
                $users[$user->id] = $user->name.' ('.$user->email.')';
            }
            
            $products = Product::all();
            $tags = [];
            
            foreach ($products as $product) {
                $product->tags = unserialize($product->tags);
                
                foreach ($product->tags as $tag) {
                    if (!in_array($tag, $tags)) $tags[$tag] = $tag;
                }
                
            }
            
            return view('products.create',compact('tags', 'users'));
        }
        
        /*
         * La funcion store() guarda una entrada en la Base de Datos luego de hacer ciertas validaciones.
         * Se puede apreciar el manejo de imagenes, que son subidas al directorio image/ en el directorio publico de Laravel.
         */
        public function store(Request $request)
        {
            $request->validate([
                'name' => 'required',
                'detail' => 'required',
                'price'=>'required',
                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg,webp|max:2048',
                'tags' => 'required',
            ]); 
    
            $input = $request->all();  
    
            if ($image = $request->file('image')) {
                $destinationPath = 'image/';
                $productImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
                $image->move($destinationPath, $productImage);
                $input['image'] = "$productImage";
            }
            
            $input['tags'] = serialize($input['tags']);
            $input['owner'] = ($request->owner) ? $request->owner : Auth::user()->id;
            $input['status'] = 'revision';
    
            Product::create($input);        
    
            return redirect()->route('products.index')->with('alert', 'success')->with('message','Producto creado correctamente');
        }
        
        /*
         * La funcion show() muestra una vista con los datos del producto sin mayores aclaraciones.
         */
        public function show(Product $product)
        {
            $product->tags = unserialize($product->tags);
                
            return view('products.show',compact('product'));
        }
        
        /*
         * La funcion edit() muestra el formulario para editar un producto.
         * Se discrimina entre usuarios admin/mod y clientes, pues estos ultimos solo pueden editar productos donde son propietarios.
         */
        public function edit(Product $product)
        {
            if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador') || $product->owner == Auth::user()->id) {
                $users_query = User::all();
                $users = array();
                
                foreach ($users_query as $user) {
                    $users[$user->id] = $user->name.' ('.$user->email.')';
                }
                
                $product->tags = unserialize($product->tags);
                    
                return view('products.edit',compact('product', 'users'));
            } else {
                return redirect()->back()->with('alert', 'danger')->with('message','No tiene permisos para editar este producto.');
            }
        }  
        
        /*
         * La funcion update() actualiza la entrada en la BD sin mayores aclaraciones pues es similiar a store().
         */
        public function update(Request $request, Product $product)
        {
            if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador') || $product->owner == Auth::user()->id) {
                $request->validate([
                    'name' => 'required',
                    'detail' => 'required',
                    'price'=>'required',
                    'tags' => 'required',
                ]);
                
                $input = $request->all();  
                
                if ($image = $request->file('image')) {
                    $destinationPath = 'image/';
                    $productImage = date('YmdHis') . "." . $image->getClientOriginalExtension();
                    $image->move($destinationPath, $productImage);
                    $input['image'] = "$productImage";
                }else{
                    unset($input['image']);
                }          
                
                $input['tags'] = serialize($input['tags']);
                $input['owner'] = ($request->owner) ? $request->owner : Auth::user()->id;
                
                $product->update($input);
                
                return redirect()->route('products.index')->with('alert', 'success')->with('message','Producto actualizado correctamente');
            } else {
                return redirect()->back()->with('alert', 'danger')->with('message','No tiene permisos para actualizar este producto.');
            }
        }
        
        /*
         * La funcion destroy() elimina la entrada en la BD y borra el archivo asociado.
         */
        public function destroy(Product $product)
        {
            if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador') || $product->owner == Auth::user()->id) {
                $ImagePath = 'image/'.$product->image;
                
                unset($ImagePath);
                
                $product->delete();
                
                return redirect()->route('products.index')->with('alert', 'success')->with('message','Producto eliminado');
            } else {
                return redirect()->back()->with('alert', 'danger')->with('message','No tiene permisos para eliminar este producto.');
            }
        }
        
        /*
         * La funcion get() con metodo GET es la que llamamos desde el Frontend mediante Ajax, devuelve el resultado codificado en JSON con json_encode()
         */
        public function get($product_id)
        {
            $product = Product::where('id', $product_id)->first();
            $product->tags = unserialize($product->tags);
            
            return json_encode($product->toArray());
        }
    }

### Archivos de vistas en **/resources/views/**
El frontend es simple, es una plantilla HTML en el archivo **front/index.blade.php** donde se puede destacar la combinacion de PHP mediante Blade, Javascript y jQuery, este último utilizando Ajax para mejorar la experiencia del usuario manipulando contenido dinamico sin que haya que recargar la página (se ve en el video el funcionamiento).

    <script>
		$('.js-show-modal1').on('click',function(e){
			e.preventDefault();

			$.get("{{ url('/products/get') }}"+"/"+$(this).attr('data-product_id'))
				.done(function( response ) {
					var data = JSON.parse(response);
					var image_path = "{{ url('public/image') }}";

					$('#product_id').text(data.id);
					$('#product_owner').text(data.owner);
					$('#product_name').text(data.name);
					$('#product_price').text(data.price);
					$('#product_detail').text(data.detail);
					$('#product_image').attr('src', image_path + '/' + data.image);
				});
				
				$('.js-modal1').addClass('show-modal1');
			});

			$('.js-hide-modal1').on('click',function(){
			$('.js-modal1').removeClass('show-modal1');
		});

		$('.js-addcart-detail').on('click',function(e){
			e.preventDefault();

			$.post("{{ url('/orders/store') }}", { 
				_token: $("input[name=_token").val(),
				from: {{ Auth::user()->id }},
				to: $('#product_owner').text(),
				product: $('#product_id').text(),
				quantity: $('#quantity').val(),
				message: $('#message').val()
				}).done(function( response ) {
					var data = JSON.parse(response);
					$('.response').parent().parent().addClass('show-modal-search');
					
					$('.response').html(`
						<p>Muy bien! Tu pedido fue enviado al Vendedor, aquí los datos:<p>
						<p><strong>Producto:</strong> `+data.content['product_name']+`</p>
						<p><strong>Cantidad:</strong> `+data.content['quantity']+` | 
						<strong>Monto:</strong> $ `+data.content['amount']+`</p>
						<p><strong>Vendedor:</strong> `+data.to+`</p>
						<p><strong>Comprador:</strong> `+data.from+`</p>
						<p><strong>Fecha:</strong> `+data.created_at+`</p>
					`);
					
				$('.js-modal1').removeClass('show-modal1');
			});
		});

		$('.js-hide-modal-search').on('click', function(){
			$('.modal-search-header').removeClass('show-modal-search');
			$('.js-show-modal-search').css('opacity','1');
		});
    </script>

El backend es diferente, tiene una estructura que se puede apreciar en **/layouts/app.blade.php** donde cargamos las herramientas utilizadas en nuestro proyecto: Boostsrap, jQuery, DataTables, Boostsrap Icons, se pueden apreciar funciones Blade para el manejo de PHP directo sobre la plantilla (como @guest o @yield)

    <!DOCTYPE html>
    <html lang="{{ app()->getLocale() }}">
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        
        <title>{{ config('app.name') }}</title>
        
        <!-- Bootstrap Core -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- Bootsrap Icons --->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
        <!-- Select2 Styles -->
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <!-- DataTables Styles -->
        <link href="https://cdn.datatables.net/1.12.0/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
        <!-- Local CSS styles -->
        <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        </head>
        <body>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name') }}</a>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @guest
                            <li><a class="nav-link" href="{{ route('login') }}">Ingresar</a></li>
                            <li><a class="nav-link" href="{{ route('register') }}">Registro</a></li>
                        @else
                            @can('user-list')
                            <li><a class="nav-link" href="{{ route('users.index') }}">Usuarios</a></li>
                            @endcan
                            @can('role-list')
                            <li><a class="nav-link" href="{{ route('roles.index') }}">Roles</a></li>
                            @endcan
                            @can('product-list')
                            <li><a class="nav-link" href="{{ route('products.index') }}">Productos</a></li>
                            @endcan
                            @can('order-list')
                            <li><a class="nav-link" href="{{ route('orders.index') }}">Pedidos</a></li>
                            @endcan
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="bi bi-person-circle"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('users.show', Auth::id()) }}">Perfil</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Salir</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </nav>
            <div class="container-fluid mt-5 pl-5 pr-5">
                @yield('content') // aqui es donde incluímos los contenidos de cada vista particular
                <footer class="py-3 my-4">
                    <hr>
                    <p class="text-center text-muted">© 2022 ClassificadoX</p>
                </footer>
            </div>
            
            <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"  crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
            <script src="https://cdn.datatables.net/1.12.0/js/jquery.dataTables.min.js"></script>
            <script src="https://cdn.datatables.net/1.12.0/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
            <script src="{{ asset('public/js/app.js') }}"></script>
            @yield('scripts') // aqui es donde incluímos las secciones javascripts de cada vista particular
        </body>
    </html>

Y para finalizar un par de vistas concretas **/products/index.blade.php** y **/products/edit.blade.php**

    // Archivo products/index.blade.php
	@extends('layouts.app') // informa a Blade que esta plantilla es una extensión de /layouts/app.blade.php
    @section('content') // todo lo que este dentro de estas etiquetas, aparecerá dentro de @yield('content) en app.blade.php
        <div class="row">
            <div class="col-12">
                <div class="float-left">
                    <h2>Productos</h2>
                </div>
                <div class="float-right">
                    @can('product-create')
                    <a class="btn btn-success" href="{{ route('products.create') }}">Crear Producto</a>
                    @endcan
                </div>
            </div>
        </div>
        @if (Session::get('message'))
            <div class="alert alert-{{Session::get('alert')}}">
                {{ Session::get('message') }}
            </div>
        @endif
        <table class="table table-sm table-bordered">
            <thead>
                <tr>
                    <th>ID</th>            
                    <th>Foto</th>
                    <th>Nombre</th>
                    <th>Detalles</th>
                    <th>Tags</th>
                    <th>Precio</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
        	    <tr>
        	        <td>{{ $product->id }}</td>
                    <td><img src="{{ url('/public/image', $product->image) }}" class="img-thumbnail width-150"></td>
        	        <td>{{ $product->name }}</td>
        	        <td>{{ $product->detail }}</td>
        	        <td>@foreach ($product->tags as $tag) <label class="badge badge-primary">{{ $tag }}</label> @endforeach</td>
                    <td>{{ $product->price }}</td>
        	        <td>            
                        <a class="btn btn-sm btn-info" href="{{ route('products.show',$product->id) }}"><i class="bi bi-eye"></i></a>
                        @can('product-edit')
                        <a class="btn btn-sm btn-primary" href="{{ route('products.edit',$product->id) }}"><i class="bi bi-pencil-square"></i></a>
                        @endcan                
                        @can('product-delete')
                            {!! Form::open(['method' => 'DELETE','route' => ['products.destroy', $product->id],'style'=>'display:inline']) !!}
                                {!! Form::button('<i class="bi bi-trash"></i>', ['class' => 'btn btn-sm btn-danger', 'type' => 'submit']) !!}
                            {!! Form::close() !!}
                        @endcan            
        	        </td>
        	    </tr>
        	    @endforeach
            </tbody>
        </table>
    @endsection
    @section('scripts')
    <script>
        $(document).ready(function() {
            $('.table').DataTable({
                responsive: true
            });
        });
    </script>
    @endsection

-

    // Archivo products/edit.blade.php
    @extends('layouts.app')
    @section('content')
        <div class="row">
            <div class="col-12">
                <div class="float-left">
                    <h2>Editar Producto</h2>
                </div>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ route('products.index') }}"> Atras</a>
                </div>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Ups!</strong> Hubo algunos problemas con tus datos ingresados.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('products.update',$product->id) }}" method="POST" enctype="multipart/form-data">
        	@csrf
            @method('PUT')
             <div class="row">
    		    <div class="col-12">
    		        <div class="form-group">
    		            <strong>Nombre:</strong>
    		            <input type="text" name="name" value="{{ $product->name }}" class="form-control" placeholder="Nombre">
    		        </div>
    		    </div>
    		    <div class="col-12">
    		        <div class="form-group">
    		            <strong>Detalles:</strong>
    		            <textarea class="form-control" style="height:150px" name="detail" placeholder="Detalles">{{ $product->detail }}</textarea>
    		        </div>
    		    </div>
    		    <div class="col-6">
    		        <div class="form-group">
    		            <strong>Precio:</strong>
    		            <input type="number" name="price" value="{{ $product->price }}" class="form-control" placeholder="Precio">
    		        </div>
    		    </div>
    		    <div class="col-6">
    		        <div class="form-group">
    		            <strong>Tags:</strong>
    		            <select name="tags[]" class="form-control form-control-select" multiple="multiple">
    		                @foreach ($product->tags as $tag)
    		                <option value="{{ $tag }}" selected>{{ $tag }}</option>
    		                @endforeach
    		            </select>
    		        </div>
    		    </div>  
                @if (Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Moderador'))
    		    <div class="col-6">
                    <div class="form-group">
                        <strong>Foto:</strong>
                        {!! Form::file('image', null) !!}                    
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <strong>Autor:</strong>
                        {!! Form::select('owner', $users, $product->owner, ['class' => 'form-control', 'required' => 'required']) !!}                  
                    </div>
                </div>
    		    @else
                <div class="col-12">
                    <div class="form-group">
                        <strong>Foto:</strong>
                        {!! Form::file('image', null) !!}                    
                    </div>
                </div>
                @endif
    		    <div class="col-12 text-center">
    		      <button type="submit" class="btn btn-primary">Guardar</button>
    		    </div>
    		</div>
        </form>
    @endsection
    @section('scripts')
    <script>
        $(document).ready( function () {
            $('select').select2({
                maximumSelectionLength: 10,
                tags: true
            });
        });
    </script>
    @endsection

